package com.example.loteria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText num1;
    private EditText numerocomprobacion;
    private Button btnComprobar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1 = findViewById(R.id.txtlimite);
        numerocomprobacion = findViewById(R.id.txtnumero);
        btnComprobar = findViewById(R.id.botcomprobar);

        btnComprobar.setOnClickListener(this);


    }
    @Override
    public void onClick(View v){
        if (v == btnComprobar){
            if (num1.getText().toString().isEmpty()){
                num1.setError("Escribe el limite de numeros");
                num1.requestFocus();

            }else if(numerocomprobacion.getText().toString().isEmpty()){
                numerocomprobacion.setError("Digite el numero a comprobar");
                numerocomprobacion.requestFocus();


            }else{
                int lim = Integer.parseInt(num1.getText().toString());
                int com = Integer.parseInt(numerocomprobacion.getText().toString());
                if (com > lim){
                    numerocomprobacion.setError("Dijite un numero menor o igual al limite");

                }else{
                    Intent intent = new Intent(this, Comprobacion.class);

                    intent.putExtra("dato1", num1.getText().toString());
                    intent.putExtra("dato2", numerocomprobacion.getText().toString());
                    startActivity(intent);

                }
            }

        }




    }


}