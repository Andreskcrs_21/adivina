package com.example.loteria;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Comprobacion extends AppCompatActivity {
    private TextView resultado;
    String num1, numeroComprobacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprobacion);

        resultado = findViewById(R.id.txtresultado);

        num1 = getIntent().getStringExtra("dato1");
        numeroComprobacion = getIntent().getStringExtra("dato2");
        Comprobar();
    }
    public int numeroAleatorio(int num){
        int aleatorio = (int) (Math.random()* num);
        return aleatorio;

    }
    public void Comprobar(){
        int Ncomprobacion = Integer.parseInt(numeroComprobacion);
        int Nlimite = Integer.parseInt(num1);
        int random = numeroAleatorio(Nlimite);
        if (Ncomprobacion == random){
            resultado.setText("Felicidades, ganaste.  El numero aleatorio era: "+random);





        }else{
            resultado.setText("Perdiste \n El numero aleatorio era:"+random);
        }

    }

    public void VolverJugar(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


}